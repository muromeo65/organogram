module Types
  class MutationType < Types::BaseObject
    field :create_company,    mutation: Mutations::CreateCompany
    field :delete_employee,   mutation: Mutations::DeleteEmployee
    field :create_employee,   mutation: Mutations::CreateEmployee
    field :associate_manager, mutation: Mutations::AssociateManager
  end
end
