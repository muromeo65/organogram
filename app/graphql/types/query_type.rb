module Types
  class QueryType < Types::BaseObject
    field :companies,                       resolver: Queries::Companies
    field :get_company,                     resolver: Queries::GetCompany
    field :employees,                       resolver: Queries::Employees
    field :pairs,                           resolver: Queries::Pairs
    field :managed_employees,               resolver: Queries::ManagedEmployees
    field :second_level_managed_employees,  resolver: Queries::SecondLevelManagedEmployees
  end
end
