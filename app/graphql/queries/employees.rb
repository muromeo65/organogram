module Queries
  class Employees < Queries::BaseQuery
    description 'Get a list of all employees from a company'

    argument :company_id, String, required: true

    type [Types::EmployeeType], null: true

    def resolve(args)
      result = Employee::ListAllByCompany.call(args)

      result.success? ? result.employees : print_error(result.error)
    end
  end
end
