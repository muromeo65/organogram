module Queries
  class SecondLevelManagedEmployees < Queries::BaseQuery
    description 'Get the second level of managed_employees from a manager'

    argument :employee_id, String, required: true

    type [Types::EmployeeType], null: true

    def resolve(args)
      result = Organization::SecondLevelOfManagedEmployees.call(args)

      result.success? ? result.managed_employees : print_error(result.error)
    end
  end
end
