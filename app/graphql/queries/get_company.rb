module Queries
  class GetCompany < Queries::BaseQuery
    description 'Get a company by id or by name'

    argument :search, String, required: true

    type Types::CompanyType, null: true

    def resolve(args)
      result = Company::Search.call(args)

      result.success? ? result.company : print_error(result.error)
    end
  end
end
