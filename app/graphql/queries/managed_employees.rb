module Queries
  class ManagedEmployees < Queries::BaseQuery
    description 'Get the managed_employees from a manager'

    argument :employee_id, String, required: true

    type [Types::EmployeeType], null: true

    def resolve(args)
      result = Organization::ManagedEmployeesByManager.call(args)

      result.success? ? result.managed_employees : print_error(result.error)
    end
  end
end
