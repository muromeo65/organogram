module Queries
  class Companies < Queries::BaseQuery
    description 'List all companies in database'

    type [Types::CompanyType], null: true

    def resolve
      result = Company::ListAll.call()

      result.success? ? result.companies : print_error(result.error)
    end
  end
end
