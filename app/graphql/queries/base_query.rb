module Queries
  class BaseQuery < GraphQL::Schema::Resolver
    private

    def print_error(message)
      GraphQL::ExecutionError.new(message)
    end
  end
end
