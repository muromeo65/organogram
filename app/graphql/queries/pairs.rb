module Queries
  class Pairs < Queries::BaseQuery
    description 'Get a list of the pairs of employees from a employee'

    argument :employee_id, String, required: true

    type [Types::EmployeeType], null: true

    def resolve(args)
      result = Organization::EmployeePairs.call(args)

      result.success? ? result.employees : print_error(result.error)
    end
  end
end
