module Mutations
  class CreateCompany < Mutations::BaseMutation
    description 'Create a new company'

    argument :name, String, required: true

    field :company, Types::CompanyType, null: false

    def resolve(args)
      result = Company::Create.call(args)

      result.success? ? { company: result.company } : print_error(result.error)
    end
  end
end
