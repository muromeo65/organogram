module Mutations
  class DeleteEmployee < Mutations::BaseMutation
    description 'Delete a employee by his id'

    argument :employee_id, String, required: true

    field :deleted, Boolean, null: false

    def resolve(args)
      result = Employee::Delete.call(args)

      result.success? ? { deleted: result.deleted } : print_error(result.error)
    end
  end
end
