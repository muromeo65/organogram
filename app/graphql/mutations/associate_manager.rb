module Mutations
  class AssociateManager < Mutations::BaseMutation
    description 'Associate an employee as manager of another passing they ids'

    argument :employee_id, String, required: true
    argument :manager_id,  String, required: true
    argument :company_id,  String, required: true

    field :employee, Types::EmployeeType, null: false

    def resolve(args)
      result = Organization::SetManager.call(args)

      result.success? ? { employee: result.employee } : print_error(result.error)
    end
  end
end
