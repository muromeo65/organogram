module Mutations
  class CreateEmployee < Mutations::BaseMutation
    description 'Create a new employee'

    argument :name,       String, required: true
    argument :email,      String, required: true
    argument :company_id, String, required: true

    field :employee, Types::EmployeeType, null: false

    def resolve(args)
      result = Employee::Create.call(args)

      result.success? ? { employee: result.employee } : print_error(result.error)
    end
  end
end
