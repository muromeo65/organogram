class Employee < ApplicationRecord
  has_many   :managed_employees, class_name: 'Employee', foreign_key: 'manager_id'
  belongs_to :manager, class_name: 'Employee', optional: true
  belongs_to :company
end
