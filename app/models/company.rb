class Company < ApplicationRecord
  has_many :employees
  before_validation :create_slug, only: [:create]

  private

  def create_slug
    self.slug = name.to_s.parameterize
  end
end
