class Company::Search
  include Interactor

  def call
    raise 'search field cant be nil' if context.search.blank?

    context.search.match(/^[0-9]+$/) ? search_by_id : search_by_name
  rescue => e
    context.fail!(error: e.message)
  end

  private

  def search_by_id
    context.company = Company.where(id: context.search.to_i).limit(1)[0]
  end

  def search_by_name
    context.company = Company.where(slug: context.search.to_s.parameterize).limit(1)[0]
  end
end
