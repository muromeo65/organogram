class Company::Create
  include Interactor

  def call
    raise 'name cant be null' if context.name.blank?

    company = Company.where(slug: context.name.to_s.parameterize).limit(1)[0]
    raise 'company already exists' if company.present?

    context.company = Company.create!(name: context.name)
  rescue => e
    context.fail!(error: e.message)
  end
end
