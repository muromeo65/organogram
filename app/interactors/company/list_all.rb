class Company::ListAll
  include Interactor

  def call
    context.companies = []

    Company.where('id <> 0').order('name').each_row { |hash| context.companies << hash }
  rescue => e
    context.fail!(error: e.message)
  end
end
