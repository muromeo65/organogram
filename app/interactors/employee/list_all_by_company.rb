class Employee::ListAllByCompany
  include Interactor

  def call
    raise 'company_id cant be nil' if context.company_id.blank?

    company = Company.where(id: context.company_id).limit(1)[0]
    raise 'Company not found' if company.blank?

    context.employees = []

    company.employees.each_row { |hash| context.employees << hash }
  rescue=> e
    context.fail!(error: e.message)
  end
end
