class Employee::Create
  include Interactor

  def call
    raise 'company_id cant be nil' if context.company_id.blank?
    raise 'name cant be nil'       if context.name.blank?
    raise 'email cant be nil'      if context.email.blank?

    employee = Employee.where(email: email).limit(1)[0]
    raise 'employee already exists' if employee.present?

    context.employee = Employee.create!(
      name:       context.name,
      email:      email,
      company_id: context.company_id
    )
  rescue => e
    context.fail!(error: e.message)
  end

  private

  def email
    context.email.downcase
  end
end
