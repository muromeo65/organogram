class Employee::Delete
  include Interactor

  def call
    raise 'employee_id cant be nil' if context.employee_id.blank?

    employee = Employee.where(id: context.employee_id).limit(1)[0]
    raise 'employee doesnt exists' if employee.blank?

    employee.delete ? context.deleted = true : context.deleted = false
  rescue => e
    context.fail!(error: e.message)
  end
end
