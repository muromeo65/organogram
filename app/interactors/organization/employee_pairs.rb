class Organization::EmployeePairs
  include Interactor

  def call
    raise 'employee_id cant be nil' if context.employee_id.blank?

    employee = Employee.where(id: context.employee_id).limit(1)[0]
    raise 'employee doesnt exists' if employee.blank?

    context.employees = employee.manager&.managed_employees
  rescue => e
    context.fail!(error: e.message)
  end
end
