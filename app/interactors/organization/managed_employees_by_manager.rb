class Organization::ManagedEmployeesByManager
  include Interactor

  def call
    raise 'employee_id cant be nil' if employee_blank?
    raise 'employee doesnt exists'  if get_employee.blank?

    context.managed_employees = @employee.managed_employees.present? ? @employee.managed_employees : nil
  rescue => e
    context.fail!(error: e.message)
  end

  private

  def employee_blank?
    context.employee_id.blank? && context.employee.blank?
  end

  def get_employee
    @employee ||= context.employee.present? ? context.employee : Employee.where(id: context.employee_id).limit(1)[0]
  end
end
