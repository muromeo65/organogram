class Organization::SecondLevelOfManagedEmployees
  include Interactor

  def call
    raise 'employee_id cant be nil' if context.employee_id.blank?

    result = Organization::ManagedEmployeesByManager.call(employee_id: context.employee_id)
    raise 'employee doesnt have managed_employees' if result.managed_employees.blank?

    context.managed_employees = []

    result.managed_employees.each do |employee|
      set_managed_employees(employee)
    end
  rescue => e
    context.fail!(error: e.message)
  end

  private

  def set_managed_employees(employee)
    result = Organization::ManagedEmployeesByManager.call(employee: employee)
    return if result.managed_employees.blank?

    result.managed_employees.each { |managed_employee| context.managed_employees << managed_employee }
  end
end
