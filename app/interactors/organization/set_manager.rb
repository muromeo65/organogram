class Organization::SetManager
  include Interactor

  def call
    raise 'company_id cant be nil'  if context.company_id.blank?
    raise 'manager_id cant be nil'  if context.manager_id.blank?
    raise 'employee_id cant be nil' if context.employee_id.blank?

    company = Company.where(id: context.company_id).limit(1)[0]
    raise 'company doesnt exists'   if company.blank?

    employee = company.employees.where(id: context.employee_id).limit(1)[0]
    manager  = company.employees.where(id: context.manager_id).limit(1)[0]

    validate_conditions(employee, manager)

    create_association(employee, manager)
  rescue => e
    context.fail!(error: e.message)
  end

  private

  def validate_conditions(employee, manager)
    raise 'employees isnt from the same company'  if [employee, manager].include?(nil)
    raise 'employee already have manager'         if employee.manager.present?
    raise 'employee cant be leader of his leader' if broken_hierarchy?(employee, manager)
  end

  def create_association(employee, manager)
    employee.manager_id = manager.id

    context.employee = employee.save! ? employee : nil
  end

  def broken_hierarchy?(employee, manager)
    get_hierarchy_ids(employee, manager)

    context.chain.include?(employee.id)
  end

  def get_hierarchy_ids(employee, manager)
    context.chain ||= [manager.id]

    return if [manager, manager.manager].include?(nil)
    context.chain << manager.manager.id

    get_hierarchy_ids(employee, manager.manager)
  end
end
