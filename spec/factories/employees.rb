FactoryBot.define do
  factory :employee, class: Employee do
    name       { Faker::Name.name }
    email      { Faker::Internet.email }
    company_id { create(:company).id }
    manager_id { nil }
  end
end
