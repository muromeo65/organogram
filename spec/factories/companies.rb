FactoryBot.define do
  factory :company, class: Company do
    name { Faker::Company.name }
  end
end
