require 'rails_helper'

RSpec.describe Employee::ListAllByCompany, type: :interactor do
  describe '.call' do
    let!(:company_without_employees) { create(:company) }
    let!(:company)                   { create(:company) }

    (1..10).each do |i|
      let!(:"employee_#{i}") { create(:employee, company_id: company.id) }
    end

    context 'When I dont pass the company_id' do
      subject { described_class.call() }

      it 'should return error' do
        expect(subject).to be_a_failure
      end

      it 'should return the error message' do
        expect(subject.error).to match('company_id cant be nil')
      end
    end

    context 'When I pass the company_id' do
      subject { described_class.call(company_id: company.id) }

      it 'should return success' do
        expect(subject).to be_a_success
      end

      it 'should return employees from a company' do
        expect(subject.employees.size).to eq(10)
      end
    end

    context 'when I dont have employees' do
      subject { described_class.call(company_id: company_without_employees.id) }

      it 'should return success' do
        expect(subject).to be_a_success
      end

      it 'should return nil' do
        expect(subject.employeess).to eq(nil)
      end
    end
  end
end
