require 'rails_helper'

RSpec.describe Employee::Delete, type: :interactor do
  describe '.call' do
    let!(:employee) { create(:employee) }

    context 'When I dont pass the employee_id' do
      subject { described_class.call() }

      it 'should return error' do
        expect(subject).to be_a_failure
      end

      it 'should return the error message' do
        expect(subject.error).to match('employee_id cant be nil')
      end
    end

    context 'When employee doesnt exists' do
      subject { described_class.call(employee_id: '0') }

      it 'should return error' do
        expect(subject).to be_a_failure
      end

      it 'should return the error message' do
        expect(subject.error).to match('employee doesnt exists')
      end
    end

    context 'When the employee exists' do
      subject { described_class.call(employee_id: employee.id.to_s) }

      it 'should return success' do
        expect(subject).to be_a_success
      end

      it 'should delete the employee' do
        subject

        deleted_employee = Employee.where(id: employee.id).first
        expect(deleted_employee).to eq(nil)
      end

      it 'should return the delete confirmation' do
        expect(subject.deleted).to eq(true)
      end
    end
  end
end
