require 'rails_helper'

RSpec.describe Employee::Create, type: :interactor do
  describe '.call' do
    let!(:company) { create(:company) }
    let!(:name)    { Faker::Name.name }
    let!(:email)   { Faker::Internet.email(name: name, separators: '_') }

    context 'When I pass the parameters' do
      subject { described_class.call(name: name, email: email, company_id: company.id) }

      it 'should be a success' do
        expect(subject).to be_a_success
      end

      it 'should create the employee' do
        expect(subject.employee).to be_present
      end

      it 'should save the name' do
        expect(subject.employee.name).to eq(name)
      end

      it 'should save the email' do
        expect(subject.employee.email).to eq(email)
      end
    end

    context 'When I dont pass the email' do
      subject { described_class.call(name: name, company_id: company.id) }

      it 'should return error' do
        expect(subject).to be_a_failure
      end

      it 'should return the error message' do
        expect(subject.error).to match('email cant be nil')
      end
    end

    context 'When I dont pass the name' do
      subject { described_class.call(email: email, company_id: company.id) }

      it 'should return error' do
        expect(subject).to be_a_failure
      end

      it 'should return the error message' do
        expect(subject.error).to match('name cant be nil')
      end
    end

    context 'When I dont pass the company_id' do
      subject { described_class.call(name: name, email: email) }

      it 'should return error' do
        expect(subject).to be_a_failure
      end

      it 'should return the error message' do
        expect(subject.error).to match('company_id cant be nil')
      end
    end
  end
end
