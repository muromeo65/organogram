require 'rails_helper'

RSpec.describe Organization::SetManager, type: :interactor do
  describe '.call' do
    let!(:company)    { create(:company) }
    let!(:manager)    { create(:employee, company_id: company.id) }
    let!(:employee)   { create(:employee, company_id: company.id, manager_id: manager.id) }
    let!(:employee_1) { create(:employee) }

    let!(:founder)     { create(:employee, company_id: company.id) }
    let!(:cto)         { create(:employee, company_id: company.id) }

    context 'When I dont pass the company_id' do
      subject { described_class.call(employee_id: employee.id, manager_id: manager.id) }

      it 'should return error' do
        expect(subject).to be_a_failure
      end

      it 'should return the error message' do
        expect(subject.error).to match('company_id cant be nil')
      end
    end

    context 'When I dont pass the manager_id' do
      subject { described_class.call(employee_id: employee.id, company_id: company.id) }

      it 'should return error' do
        expect(subject).to be_a_failure
      end

      it 'should return the error message' do
        expect(subject.error).to match('manager_id cant be nil')
      end
    end

    context 'When I dont pass the employee_id' do
      subject { described_class.call(manager_id: manager.id, company_id: company.id) }

      it 'should return error' do
        expect(subject).to be_a_failure
      end

      it 'should return the error message' do
        expect(subject.error).to match('employee_id cant be nil')
      end
    end

    context 'When company doesnt exists' do
      subject { described_class.call(manager_id: manager.id, employee_id: employee.id, company_id: '0') }

      it 'should return error' do
        expect(subject).to be_a_failure
      end

      it 'should return the error message' do
        expect(subject.error).to match('company doesnt exists')
      end
    end

    context 'When the employees are from distinct companies' do
      subject { described_class.call(manager_id: manager.id, company_id: company.id, employee_id: employee_1.id) }

      it 'should return error' do
        expect(subject).to be_a_failure
      end

      it 'should return the error message' do
        expect(subject.error).to match('employees isnt from the same company')
      end
    end

    context 'When the employee already have a manager' do
      subject { described_class.call(manager_id: manager.id, company_id: company.id, employee_id: employee.id) }

      it 'should return error' do
        expect(subject).to be_a_failure
      end

      it 'should return the error message' do
        expect(subject.error).to match('employee already have manager')
      end
    end

    context 'When the employee doesnt have a manager' do
      subject { described_class.call(company_id: company.id, manager_id: founder.id, employee_id: cto.id) }

      it 'should return success' do
        expect(subject).to be_a_success
      end

      it 'should have founder as manager of cto' do
        expect(subject.employee.manager).to eq(founder)
      end

      it 'should have cto as managed_employee of founder' do
        expect(subject.employee).to eq(founder.managed_employees[0])
      end
    end

    context 'When I set employee as manager of manager' do
      subject { described_class.call(company_id: company.id, manager_id: employee.id, employee_id: manager.id) }

      it 'should return error' do
        expect(subject).to be_a_failure
      end

      it 'should return the error message' do
        expect(subject.error).to eq('employee cant be leader of his leader')
      end
    end
  end
end
