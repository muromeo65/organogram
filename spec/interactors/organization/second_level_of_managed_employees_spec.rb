require 'rails_helper'

RSpec.describe Organization::SecondLevelOfManagedEmployees, type: :interactor do
  describe '.call' do
    let!(:company)    { create(:company) }

    let!(:manager)    { create(:employee, company_id: company.id) }
    let!(:employee_1) { create(:employee, company_id: company.id, manager_id: manager.id) }
    let!(:employee_2) { create(:employee, company_id: company.id, manager_id: manager.id) }

    let!(:employee_of_1) { create(:employee, company_id: company.id, manager_id: employee_1.id) }
    let!(:employee_of_2) { create(:employee, company_id: company.id, manager_id: employee_2.id) }

    context 'When I dont pass the employee_id' do
      subject { described_class.call() }

      it 'should return error' do
        expect(subject).to be_a_failure
      end

      it 'should return the error message' do
        expect(subject.error).to match('employee_id cant be nil')
      end
    end

    context 'When I pass the employee_id' do
      subject { described_class.call(employee_id: manager.id) }

      it 'should return success' do
        expect(subject).to be_a_success
      end

      it 'should return 2 managed employees' do
        expect(subject.managed_employees.size).to eq(2)
      end

      it 'should contain the 2 employee_id of the second level' do
        expect(subject.managed_employees.map(&:id)).to include(employee_of_1.id, employee_of_2.id)
      end
    end
  end
end
