require 'rails_helper'

RSpec.describe Organization::ManagedEmployeesByManager, type: :interactor do
  describe '.call' do
    let!(:company)                               { create(:company) }
    let!(:employee_without_managed_employees)    { create(:employee, company_id: company.id) }

    let!(:manager)    { create(:employee, company_id: company.id) }
    let!(:employee_1) { create(:employee, company_id: company.id, manager_id: manager.id) }
    let!(:employee_2) { create(:employee, company_id: company.id, manager_id: manager.id) }

    context 'When I dont pass the employee_id' do
      subject { described_class.call() }

      it 'should return error' do
        expect(subject).to be_a_failure
      end

      it 'should return the error message' do
        expect(subject.error).to match('employee_id cant be nil')
      end
    end

    context 'When I pass the employee_id' do
      subject { described_class.call(employee_id: manager.id) }

      it 'should return success' do
        expect(subject).to be_a_success
      end

      it 'should return 2 managed employees' do
        expect(subject.managed_employees.size).to eq(2)
      end
    end

    context 'When the employee doenst have managed_employees' do
      subject { described_class.call(employee_id: employee_without_managed_employees.id) }

      it 'should return success' do
        expect(subject).to be_a_success
      end

      it 'should return nil' do
        expect(subject.managed_employees).to eq(nil)
      end
    end
  end
end
