require 'rails_helper'

RSpec.describe Organization::EmployeePairs, type: :interactor do
  describe '.call' do
    let!(:company)                  { create(:company) }
    let!(:employee_without_manager) { create(:employee, company_id: company.id) }

    let!(:leader)     { create(:employee, company_id: company.id) }
    let!(:employee_1) { create(:employee, company_id: company.id, manager_id: leader.id) }
    let!(:employee_2) { create(:employee, company_id: company.id, manager_id: leader.id) }
    let!(:employee_3) { create(:employee, company_id: company.id, manager_id: leader.id) }

    context 'When I dont pass the employee_id' do
      subject { described_class.call() }

      it 'should return error' do
        expect(subject).to be_a_failure
      end

      it 'should return the error message' do
        expect(subject.error).to match('employee_id cant be nil')
      end
    end

    context 'When I pass the employee_id' do
      subject { described_class.call(employee_id: employee_1.id) }

      it 'should return success' do
        expect(subject).to be_a_success
      end

      it 'should return 3 managed_employees' do
        expect(subject.employees.size).to eq(3)
      end
    end

    context 'When the employee doenst have manager' do
      subject { described_class.call(employee_id: employee_without_manager.id) }

      it 'should return success' do
        expect(subject).to be_a_success
      end

      it 'should return nil' do
        expect(subject.employees).to eq(nil)
      end
    end
  end
end
