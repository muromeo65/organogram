require 'rails_helper'

RSpec.describe Company::Create, type: :interactor do
  describe '.call' do
    let!(:company_name) { Faker::Company.name }
    let!(:company_slug) { company_name.to_s.parameterize }

    context 'when I dont pass the company name' do
      subject { described_class.call() }

      it 'should return error' do
        expect(subject).to be_a_failure
      end

      it 'should return the error message' do
        expect(subject.error).to match('name cant be null')
      end
    end

    context 'when I pass the company name' do
      subject { described_class.call(name: company_name) }

      it 'should be a success' do
        expect(subject).to be_a_success
      end

      it 'should return the created company' do
        expect(subject.company).to be_present
      end

      it 'slug should be present' do
        expect(subject.company.slug).to eq(company_slug)
      end
    end
  end
end
