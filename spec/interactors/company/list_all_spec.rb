require 'rails_helper'

RSpec.describe Company::ListAll, type: :interactor do
  describe '.call' do
    (1..10).each do |i|
      let!(:"company_#{i}") { create(:company, name: Faker::Company.name) }
    end

    context 'When I call the interactor' do
      subject { described_class.call }

      it 'should return all companies' do
        expect(subject.companies.size).to eq(10)
      end
    end
  end
end
