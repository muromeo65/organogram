require 'rails_helper'

RSpec.describe Company::Search, type: :interactor do
  describe '.call' do
    let!(:company) { create(:company, id: 1, name: 'Fake Company') }

    context 'When I search by name' do
      subject { described_class.call(search: 'fake company') }

      it 'should return the company' do
        expect(subject.company.name).to eq('Fake Company')
      end
    end

    context 'When I search by id' do
      subject { described_class.call(search: '1') }

      it 'should return the company' do
        expect(subject.company.name).to eq('Fake Company')
      end
    end
  end
end
