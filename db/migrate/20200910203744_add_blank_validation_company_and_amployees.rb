class AddBlankValidationCompanyAndAmployees < ActiveRecord::Migration[6.0]
  def change
    change_column_null :companies, :name, false
    change_column_null :employees, :name, false
    change_column_null :employees, :email, false
  end
end
