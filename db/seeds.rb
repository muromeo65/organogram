# Creating example companies
(1..10).each do |i|
  company = Company.create!(name: Faker::Company.name)

  # Create example employees from company
  (1..5).each do |j|
    Employee.create!(
      name: Faker::Name.name,
      email: Faker::Internet.email,
      company_id: company.id
    )
  end
end
