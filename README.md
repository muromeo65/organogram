# Organogram

A simple Graphql API to manage organogram of companies

## Prerequisites

* Ruby 2.7.1
* Rails 6.0.3.3
* PostgreSQL >= 9

## Installation

After clone the repository, install dependencies:

```
bundle
```

Change "config/database.yml" username and password of your PostgreSQL

```
default: &default
  host: localhost
  username: your_user
  password: your_password
  ...
```

Create the database and run the migrations

```
rake db:create
rake db:migrate
```

If you wish, run the seed to create company and employee examples

```
rake db:seed
```

To run, just execute

```
rails s
```

Access "localhost:3000/graphiql" to run some queries and migrations 

## Query

#### List All Companies
```
{
  companies {
    id
    name
	slug
  }
}
```

#### Get a scpecific company

Can be searched by id:
```
{
  getCompany(search: "2") {
    id
    name
    slug
  }
}
```
Or by name
```
{
  getCompany(search: "Company example") {
    id
    name
    slug
  }
}
```

#### List all employees of a specific company
```
{
  employees(companyId: "1") {
    id
    name
    email
    managerId
    companyId
  }
}
```

#### List all managed employees from an employee's manager
```
{
  pairs(employeeId: "4") {
    id
    email
    managerId
  }
}
```

#### List all managed employees of a manager
```
{
 managedEmployees(employeeId: "5") {
    id
    name
    email
  }
}

```

#### List the second level of managed employees of a manager
```
{
  secondLevelManagedEmployees(employeeId: "5") {
    id
    name
    email
  }
}
```

## Mutations

#### Create a company

Required fields: [:name]

```
mutation {
  createCompany(input: { name: "New Company" }) {
    company {
      id
      name
    }
  }
}
```

#### Create a employee

Required fields: [:name, :email, :companyId]

```
mutation {
  createEmployee(input: { name: "Name", email: "email@email.com", companyId: "2" }) {
    employee {
      id
      name
      email
    }
  }
}
```

#### Delete a employee

Required fields: [:employeeId]

```
mutation {
 deleteEmployee(input: { employeeId: "3" }) {
    deleted
  }
}
```

#### Create a manager association between employees

Required fields: [:managerId, :employeeId, :companyId]

* managerId  -> employee_id from employee who'll be the manager
* employeeId -> employee_id from the employee whi'll be the managed

```
mutation{
  associateManager(input: { managerId: "1", employeeId: "2", companyId: "3" }) {
    employee {
      id
      name
      email
      managerId
    }
  }
}
```

## Tests

All the business rule is on the interactors, so, just run the command down bellow to get a full test cover of the system core

```
rake spec:interactors
```

## Gems

* [GraphQL](https://github.com/rmosolgo/graphql-ruby)
* [Interactors](https://github.com/collectiveidea/interactor-rails)
* [RSpec](https://github.com/rspec/rspec-rails)

## Autor

* **Murilo Hernandes Romeo** - [muromeo65](https://bitbucket.org/muromeo65/)